//
//  DataRepo.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 15/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseFirestore

class DataRepo {
    static let shared = DataRepo()
    private init() { }
    
    private var movementsQuery: CollectionReference = Firestore.firestore().collection("movementsList").document("movements").collection("collection")
    private var activationsQuery: CollectionReference = Firestore.firestore().collection("movementsList").document("movements").collection("activations")
    var movements: [Movement] = []
    var categories: [Category] = []
    var activations: [Activation] = []
    
    func setup() {
        self.getActivations()
        self.getMovements()
        self.getCategories()
    }
    
    private func getActivations() {
        self.activationsQuery.getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else {
                return
            }
            
            var activations: [Activation] = []
            
            for snap in documents {
                let activation = Activation(dictionary: snap.data())
                activations.append(activation)
            }
            
            self.activations = activations
        }
    }
    
    private func getCategories() {
        // make this server side
        let olyLift = Category(name: "Olympic weightlifting", description: "Olympic discipline in which the athlete attempts a maximum-weight single life of a barball loaded with weights.")
        let gymnastics = Category(name: "Gymnastics", description: "Gymnastics descriptio here.")
        let powerlifting = Category(name: "Powerlifting", description: "Power lifting description here.")
        let fitness = Category(name: "Fitness", description: "Fitness description here.")
        
        self.categories = [olyLift, gymnastics, powerlifting, fitness]
    }
    
    private func getRecoveries() {
//        self.query.getDocuments { (snapshot, error) in
//            guard let documents = snapshot?.documents else {
//                return
//            }
//
//            var movements: [Movement] = []
//
//            for snap in documents {
//                let movement = Movement(dictionary: snap.data())
//                movements.append(movement)
//            }
//
//            print(movements)
//        }
    }
    
    private func getMovements() {
        self.movementsQuery.getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else {
                return
            }
            
            var movements: [Movement] = []
            
            for snap in documents {
                let movement = Movement(dictionary: snap.data())
                movements.append(movement)
            }
            
            self.movements = movements
        }
    }
    
    // PUBLIC FUNCTIONS
    func movementsForCategory(category: Category) -> [Movement] {
        return self.movements.filter{ $0.category == category.name }
    }
}
