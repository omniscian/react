//
//  FirebaseAuthManager.swift
//  ReAct
//
//  Created by Ian Houghton on 09/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FacebookCore
import FacebookLogin
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore

class FirebaseAuthManager {
    
    static let shared = FirebaseAuthManager()
    
    var isSignedIn: Bool {
        return Auth.auth().currentUser != nil
    }
    private var completionBlock: ((_ success: Bool) -> Void)? = nil
    
    private init() {
    }
    
    private func login(credential: AuthCredential, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().signIn(with: credential, completion: { (firebaseUser, error) in
            completionBlock(error == nil)
        })
    }
    
    func loginWithFaceBook(completionBlock: @escaping (_ success: Bool) -> Void) {
        self.completionBlock = completionBlock
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: nil, completion: didReceiveFacebookLoginResult)
    }
    
    private func didReceiveFacebookLoginResult(loginResult: LoginResult) {
        switch loginResult {
        case .success:
            didLoginWithFacebook()
        case .failed(_): break
        default: break
        }
    }
    
    fileprivate func didLoginWithFacebook() {
        
        if let accessToken = AccessToken.current {
            FirebaseAuthManager().login(credential: FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)) {[weak self] (success) in
                self?.completionBlock?(success)
            }
        }
    }
}
