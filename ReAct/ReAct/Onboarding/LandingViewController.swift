//
//  ViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 18/12/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController, RegisterEmailDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if FirebaseAuthManager.shared.isSignedIn {
            self.performSegue(withIdentifier: "LoginToHomeSegue", sender: nil)
        }        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func loginWithFacebook() {
        FirebaseAuthManager.shared.loginWithFaceBook { success in
            if success {
                self.performSegue(withIdentifier: "LoginToHomeSegue", sender: nil)
            }
        }
    }
    
    private func loginWithEmail() {
        self.performSegue(withIdentifier: "showEmailViewControllerSegue", sender: nil)
    }
    
    func registeredSuccessfully() {
        self.performSegue(withIdentifier: "LoginToHomeSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "showEmailViewControllerSegue" {
            let vc = segue.destination as? RegisterEmailViewController
            vc?.delegate = self
        }
    }
    
    // MARK: IBActions
    @IBAction func createAccountButtonPressed(sender: UIButton) {
        switch sender.tag {
        case 1:
            self.loginWithFacebook()
        case 2:
            print("facebook pressed")
        case 3:
            print("facebook pressed")
        case 4:
            self.loginWithEmail()
        case 5:
            print("facebook pressed")
        case 6:
            print("facebook pressed")
        default:
            return
        }
    }
    
}
