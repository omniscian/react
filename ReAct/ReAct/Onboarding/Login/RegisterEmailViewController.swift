//
//  EnterEmailViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 02/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit
import SnapKit

enum FormState {
    case email
    case password
    case confirmPassword
}

protocol RegisterEmailDelegate {
    func registeredSuccessfully()
}

class RegisterEmailViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var nextButton: BorderedButton!
    private var emailForm: LoginContainerView = (Bundle.main.loadNibNamed("LoginContainerView", owner: self, options: nil)?[0] as? LoginContainerView)!
    private var passwordForm: LoginContainerView = (Bundle.main.loadNibNamed("LoginContainerView", owner: self, options: nil)?[0] as? LoginContainerView)!
    private var confirmPasswordForm: LoginContainerView = (Bundle.main.loadNibNamed("LoginContainerView", owner: self, options: nil)?[0] as? LoginContainerView)!
    
    private var formState: FormState = .email
    private var passwordToConfirm: String?
    var delegate: RegisterEmailDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.configureEmailForm()
        self.configurePasswordForm()
        self.configureConfirmPasswordForm()
    }
    
    private func configureEmailForm() {
        self.emailForm.textField.delegate = self
        self.emailForm.titleLabel.text = "What's your email?"
        self.emailForm.textField.placeholder = "Email"
        self.emailForm.subtitleLabel.text = "You will need to verify your email later."
        
        self.containerView.addSubview(self.emailForm)
        self.emailForm.snp.makeConstraints { (make) in
            make.left.equalTo(self.containerView)
            make.right.equalTo(self.containerView)
            make.top.equalTo(self.containerView)
            make.bottom.equalTo(self.containerView)
        }
        
        self.emailForm.textField.text = "ian@test.com"
    }
    
    private func configurePasswordForm() {
        self.passwordForm.textField.delegate = self
        self.passwordForm.titleLabel.text = "Create a password"
        self.passwordForm.textField.placeholder = "Password"
        self.passwordForm.textField.isSecureTextEntry = true
        self.passwordForm.subtitleLabel.text = "It must contain at least 8 characters, 1 upper case, 1 special and 1 number."
        
        self.containerView.addSubview(self.passwordForm)
        self.passwordForm.snp.makeConstraints { (make) in
            make.left.equalTo(self.emailForm.snp.right)
            make.right.equalTo(self.containerView)
            make.top.equalTo(self.containerView)
            make.bottom.equalTo(self.containerView)
        }
        
        self.passwordForm.textField.text = "JazzPants123!"
    }
    
    private func configureConfirmPasswordForm() {
        self.confirmPasswordForm.textField.delegate = self
        self.confirmPasswordForm.titleLabel.text = "Confirm password"
        self.confirmPasswordForm.textField.placeholder = "Password"
        self.confirmPasswordForm.textField.isSecureTextEntry = true
        self.confirmPasswordForm.subtitleLabel.text = "Password must match."
        
        self.containerView.addSubview(self.confirmPasswordForm)
        self.confirmPasswordForm.snp.makeConstraints { (make) in
            make.left.equalTo(self.passwordForm.snp.right)
            make.right.equalTo(self.containerView)
            make.top.equalTo(self.containerView)
            make.bottom.equalTo(self.containerView)
        }
        
        self.confirmPasswordForm.textField.text = "JazzPants123!"
    }
    
    private func emailValidation() {
        guard let enteredText = self.emailForm.textField.text else {
            return
        }

        if enteredText.isValidEmail() {
            self.formState = .password
            UIView.animate(withDuration: 0.3) {
                self.emailForm.snp.updateConstraints { (make) in
                    make.left.equalTo(0 - self.emailForm.frame.size.width)
                    make.right.equalTo(0 - self.emailForm.frame.size.width)
                }
                self.passwordForm.snp.makeConstraints { (make) in
                    make.left.equalTo(self.containerView.snp.left)
                }
                self.view.layoutIfNeeded()
            }
            
            self.passwordForm.textField.becomeFirstResponder()
        } else {
            self.emailForm.textField.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 3, revert: true)
            self.emailForm.subtitleLabel.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 3, revert: true)
        }
    }
    
    private func passwordValidation() {
        guard let enteredText = self.passwordForm.textField.text else {
            return
        }
        
        if enteredText.isValidPassword() {
            self.passwordToConfirm = enteredText
            self.formState = .confirmPassword
            UIView.animate(withDuration: 0.3) {
                self.passwordForm.snp.updateConstraints { (make) in
                    make.left.equalTo(0 - self.emailForm.frame.size.width)
                    make.right.equalTo(0 - self.emailForm.frame.size.width)
                }
                self.confirmPasswordForm.snp.makeConstraints { (make) in
                    make.left.equalTo(self.containerView.snp.left)
                }
                self.view.layoutIfNeeded()
            }
            
            self.confirmPasswordForm.textField.becomeFirstResponder()
        } else {
            self.passwordForm.textField.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 3, revert: true)
            self.passwordForm.subtitleLabel.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 3, revert: true)
        }
    }
    
    private func confirmPasswordValidation() {
        guard let enteredText = self.confirmPasswordForm.textField.text else {
            return
        }
        
        if enteredText == self.passwordToConfirm {
            self.confirmPasswordForm.textField.resignFirstResponder()
            self.nextButton.startAnimate(spinnerType: .lineSpinFade, spinnercolor: .lightGray, spinnerSize: 20, complete: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.nextButton.stopAnimatingWithCompletionType(completionType: .success) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.registeredSuccessfully()
                            })
                        }
                    }
                }
                
            })
        } else {
            self.confirmPasswordForm.textField.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 3, revert: true)
            self.confirmPasswordForm.subtitleLabel.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 3, revert: true)
        }
    }
    
    @IBAction func closeButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonPressed() {
        switch formState {
        case .email:
            self.emailValidation()
        case .password:
            self.passwordValidation()
        case .confirmPassword:
            self.confirmPasswordValidation()
        }
    }
}

extension RegisterEmailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count ?? 0 > 0 {
            self.nextButtonPressed()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
