//
//  LoginContainerView.swift
//  ReAct
//
//  Created by Ian Houghton on 07/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class LoginContainerView: UIView {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var subtitleLabel: UILabel!
}
