//
//  Category.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 24/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation

struct Category {
    var name: String
    var description: String
}
