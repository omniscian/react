//
//  Movement.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 15/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation
import FirebaseFirestore

class Movement: NSObject, NSCoding, NSSecureCoding {
    var category: String
    var name: String
    var imageId: String
    var movementList: [String]
    static var supportsSecureCoding: Bool = true
    
    init(dictionary: [String: Any]) {
        self.category = dictionary["category"] as! String
        self.name = dictionary["name"] as! String
        self.imageId = dictionary["imageId"] as! String
        self.movementList = dictionary["movementList"] as! [String]
    }
    
    required init(coder aDecoder: NSCoder) {
        self.category = aDecoder.decodeObject(forKey: "category") as! String
        self.name = aDecoder.decodeObject(forKey: "name") as! String
        self.imageId = aDecoder.decodeObject(forKey: "imageId") as! String
        self.movementList = aDecoder.decodeObject(forKey: "movementList") as! [String]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.category, forKey: "category")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.imageId, forKey: "imageId")
        aCoder.encode(self.movementList, forKey: "movementList")
    }
}
