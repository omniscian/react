//
//  Activation.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 24/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation

struct Activation {
    var id: String
    var name: String
    var reps: String
    var videoId: String
    
    init(dictionary: [String: Any]) {
        self.id = dictionary["id"] as! String
        self.name = dictionary["name"] as! String
        self.reps = dictionary["reps"] as! String
        self.videoId = dictionary["videoId"] as! String
    }
}
