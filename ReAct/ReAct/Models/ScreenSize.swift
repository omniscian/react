//
//  ScreenSize.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 28/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

enum ScreenSize {
    static var bounds: CGRect { return UIScreen.main.bounds }
}
