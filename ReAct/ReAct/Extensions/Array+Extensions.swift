//
//  Array+Extensions.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 26/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation

extension Array {
    func contains<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}
