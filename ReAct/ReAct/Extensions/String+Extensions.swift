//
//  String+Extensions.swift
//  ReAct
//
//  Created by Ian Houghton on 07/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import Foundation

extension String {
    func shortString() -> String {
        let displayString = NSMutableString.init()
        var words = components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        
        if words.count > 0 {
            let firstWord = words[0]
            if firstWord.count > 0 {
                let index = firstWord.index(firstWord.startIndex, offsetBy: 1)
                displayString.append(firstWord.substring(to: index))
            }
            if words.count > 1 {
                var lastWord = words.last
                while lastWord?.count == 0 && words.count > 1 {
                    words.removeLast()
                    lastWord = words.last
                }
                
                if words.count > 1 {
                    if let last = lastWord {
                        if last.count > 0 {
                            let index = last.index(last.startIndex, offsetBy: 1)
                            displayString.append(last.substring(to: index))
                        }
                    }
                }
            }
        }
        return displayString.uppercased as String
    }
}

extension String {
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidPassword() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^(?=.*[A-Z]).(?=.*[!@#$&*]).(?=.*[0-9]).{8,}$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
