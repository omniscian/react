//
//  UIButton+Extensions.swift
//  ReAct
//
//  Created by Ian Houghton on 09/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit
import SSSpinnerButton

@IBDesignable class BorderedButton: SSSpinnerButton {

    @IBInspectable var borderColor: UIColor? {
        didSet {
            if let bColor = borderColor {
                self.layer.borderColor = bColor.cgColor
            }
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

    override var isHighlighted: Bool {
        didSet {
            guard let currentBorderColor = borderColor else {
                return
            }

            let fadedColor = currentBorderColor.withAlphaComponent(0.2).cgColor

            if isHighlighted {
                layer.borderColor = fadedColor
            } else {

                self.layer.borderColor = currentBorderColor.cgColor

                let animation = CABasicAnimation(keyPath: "borderColor")
                animation.fromValue = fadedColor
                animation.toValue = currentBorderColor.cgColor
                animation.duration = 0.4
                self.layer.add(animation, forKey: "")
            }
        }
    }
}

extension UIButton {
    func setTwoLineAttributedTitle(title: String) {
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        let buttonText: NSString = title as NSString

        let newlineRange: NSRange = buttonText.range(of: "\n")
        
        var substring1 = ""
        var substring2 = ""

        if(newlineRange.location != NSNotFound) {
            substring1 = buttonText.substring(to: newlineRange.location)
            substring2 = buttonText.substring(from: newlineRange.location)
        }

        let font1: UIFont = UIFont(name: "futurastd-boldoblique", size: 21.0)!
        let attributes1 = [NSMutableAttributedString.Key.font: font1,
                           NSMutableAttributedString.Key.foregroundColor: UIColor.white]
        let attrString1 = NSMutableAttributedString(string: substring1, attributes: attributes1)

        let font2: UIFont = UIFont(name: "AvertaStd-Bold", size: 15.0)!
        let attributes2 = [NSMutableAttributedString.Key.font: font2,
                           NSMutableAttributedString.Key.foregroundColor: UIColor.init(red: 255.0/255.0, green: 221.0/255.0, blue: 177.0/255.0, alpha: 1.0)]
        let attrString2 = NSMutableAttributedString(string: substring2, attributes: attributes2)
        attrString1.append(attrString2)
        
        self.setAttributedTitle(attrString1, for: [])
    }
}
