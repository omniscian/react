//
//  UIViewController+Extensions.swift
//  ReAct
//
//  Created by Ian Houghton on 07/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

extension UIViewController {
    open override func awakeFromNib() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let backButton = UIBarButtonItem(image: UIImage(named: "navBackButton"), style: .plain, target: self, action: #selector(popCurrentViewController))
        backButton.imageInsets = UIEdgeInsets(top: 3, left: 2, bottom: 0, right: 0)
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.leftBarButtonItem = backButton

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "AvertaStd-Bold", size: 18)!]
    }
    
    @objc private func popCurrentViewController(_ animated: Bool) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func addCloseButtonToTopLeft() {
        let backButton = UIBarButtonItem(image: UIImage(named: "blueCloseIcon"), style: .plain, target: self, action: #selector(popCurrentViewController))
        backButton.imageInsets = UIEdgeInsets(top: 3, left: 2, bottom: 0, right: 0)
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.leftBarButtonItem = backButton
    }
}
