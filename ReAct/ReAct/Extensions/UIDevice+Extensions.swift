//
//  UIDevice+Extension.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 07/09/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
