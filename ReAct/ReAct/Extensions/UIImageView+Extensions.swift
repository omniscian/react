//
//  UIImageView+Extensions.swift
//  ReAct
//
//  Created by Ian Houghton on 07/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

extension UIImageView {
    open func setImage(string: String?) {
        setImage(string: string, color: nil, circular: false, textAttributes: nil)
    }
    
    open func setImage(string: String?, color: UIColor?) {
        setImage(string: string, color: color, circular: false, textAttributes: nil)
    }
    
    open func setImage(string: String?, color: UIColor?, circular: Bool) {
        setImage(string: string, color: color, circular: circular, textAttributes: nil)
    }
    
    open func setImage(string: String?, color: UIColor?, circular: Bool, textAttributes: [NSAttributedString.Key : Any]?) {
        var displayString = ""
        if let s = string {
            displayString = s
        }
        var image:UIImage?
        if let c = color {
            image = self.imageSnap(text: displayString as String, color: c, circular: circular, textAttributes:textAttributes)
        }
        else {
            image = self.imageSnap(text: displayString as String, color: .gray, circular: circular, textAttributes: textAttributes)
        }
        if let i = image {
            self.image = i
        }
    }
    
    private func imageSnap(text: String?, color: UIColor, circular: Bool, textAttributes: [NSAttributedString.Key : Any]?) -> UIImage? {
        let scale:Float = Float(UIScreen.main.scale)
        var size:CGSize = self.bounds.size
        if (contentMode == .scaleToFill || contentMode == .scaleAspectFill || contentMode == .scaleAspectFit || contentMode == .redraw) {
            size.width = CGFloat(floorf((Float(size.width) * scale) / scale))
            size.height = CGFloat(floorf((Float(size.height) * scale) / scale))
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, CGFloat(scale))
        let context = UIGraphicsGetCurrentContext()
        if circular {
            let path = CGPath(ellipseIn: self.bounds, transform: nil)
            context!.addPath(path)
            context?.clip()
        }
        
        // Fill
        context!.setFillColor(color.cgColor)
        context!.fill(CGRect(x:0, y:0, width:size.width, height:size.height))
        
        // Text
        if let t = text {
            var ta = textAttributes
            if ta == nil {
                ta = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font : UIFont(name: "futurastd-boldoblique", size: 10)!]
            }
            let textSize:CGSize = t.size(withAttributes: ta)
            let bounds:CGRect = self.bounds
            t.draw(in: CGRect(x:bounds.size.width/2 - textSize.width/2, y:bounds.size.height/2 - textSize.height/2, width:textSize.width, height:textSize.height), withAttributes: ta)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

extension UIImage {
    func withInsets(_ insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: size.width + insets.left + insets.right,
                   height: size.height + insets.top + insets.bottom),
            false,
            self.scale)

        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return imageWithInsets
    }
}
