//
//  MobilisationView.swift
//  ReAct
//
//  Created by Ian Houghton on 09/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class MobilisationView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.3, height: 0.7)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds =  false
    }
}
