//
//  HomeViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 01/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

protocol HomeViewControllerProtocol {
    func goButtonPressed()
    func mobiliseButtonPressed()
}

class HomeViewController: UIViewController, HomeViewControllerProtocol, UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var activateButton: UIButton! {
        didSet {
            self.activateButton.setTwoLineAttributedTitle(title: "Activate\n\nWarm Up Before Your Exercises")
        }
    } 
    @IBOutlet var recoveryButton: UIButton! {
        didSet {
            self.recoveryButton.setTwoLineAttributedTitle(title: "Recover\n\nCool Down or Stretch Ater Your Exercises")
        }
    }
    
    private let repo = DataRepo.shared
    private var activationMode: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "reactBlue"), style: .plain, target: nil, action: nil)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.setImage(string: "IH", color: .gray)
        imageView.layer.cornerRadius = (imageView.frame.size.height)/2
        imageView.clipsToBounds = true
        let view = UIView(frame: imageView.frame)
        view.addSubview(imageView)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: view)
        
        self.repo.setup()
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 35, right: 0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeToActivation" {
            let activationVC = segue.destination as? ActivationViewController
            activationVC?.configure(activationMode: self.activationMode)
        }
    }
    
    @IBAction func goButtonPressed() {
        self.performSegue(withIdentifier: "HomeToMobilityTest", sender: nil)
    }
    
    @IBAction func activationButtonPressed() {
        self.activationMode = true
        self.performSegue(withIdentifier: "HomeToActivation", sender: nil)
    }
    
    @IBAction func recoveryButtonPressed() {
        self.activationMode = false
        self.performSegue(withIdentifier: "HomeToActivation", sender: nil)
    }
    
    func mobiliseButtonPressed() {
        
    }
    
}
