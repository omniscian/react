//
//  MobilityViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 09/02/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class MobilityViewController: UIViewController {
    
    @IBOutlet var mobilityScoreImageView: UIImageView! {
        didSet {
            self.mobilityScoreImageView.layer.cornerRadius = (self.mobilityScoreImageView.frame.size.height)/2
            self.mobilityScoreImageView.clipsToBounds = true
        }
    }
    @IBOutlet var mobilityScoreLabel: UILabel!
    @IBOutlet var strongPointButton: UIButton!
    @IBOutlet var strongPointLabel: UILabel!
    @IBOutlet var needsWorkButton: UIButton!
    @IBOutlet var needsWorkLabel: UILabel!
    @IBOutlet var weakPointButton: UIButton!
    @IBOutlet var weakPointLabel: UILabel!
    @IBOutlet var mobiliseButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // TODO: Custom colours here depending on score..
        self.mobilityScoreImageView.setImage(string: "82", color: .green)
        self.mobilityScoreLabel.text = "Ideal"
    }
    
    @IBAction func goButtonPressed() {
        guard let hvc = self.parent as? HomeViewControllerProtocol else {
            return
        }
        hvc.goButtonPressed()
    }
    
    @IBAction func mobiliseButtonPressed() {
        guard let hvc = self.parent as? HomeViewControllerProtocol else {
            return
        }
        hvc.mobiliseButtonPressed()
    }
}
