//
//  BeforeWeStartViewController.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 28/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class BeforeWeStartViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel! {
        didSet {
            self.titleLabel.alpha = 0.0
        }
    }
    
    private var paraOriginalConstant: CGFloat = 0.0
    @IBOutlet var para1LeadingLayoutConstraint: NSLayoutConstraint! {
        didSet {
            self.para1LeadingLayoutConstraint.constant = ScreenSize.bounds.width
        }
    }
    
    @IBOutlet var para2LeadingLayoutConstraint: NSLayoutConstraint! {
        didSet {
            self.para2LeadingLayoutConstraint.constant = ScreenSize.bounds.width
        }
    }
    
    @IBOutlet var para3LeadingLayoutConstraint: NSLayoutConstraint! {
        didSet {
            self.para3LeadingLayoutConstraint.constant = ScreenSize.bounds.width
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let backBTN = UIBarButtonItem(image: UIImage(named: "navBackButton"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        self.navigationItem.leftBarButtonItem = backBTN
        self.view.layoutIfNeeded()
        self.animate()
    }
    
    private func animate() {
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
            self.titleLabel.alpha = 1.0
        }) { _ in
            UIView.animate(withDuration: 0.3, animations: {
                self.para1LeadingLayoutConstraint.constant = self.paraOriginalConstant
                self.view.layoutIfNeeded()
            }) { _ in
                UIView.animate(withDuration: 0.3, animations: {
                    self.para2LeadingLayoutConstraint.constant = self.paraOriginalConstant
                    self.view.layoutIfNeeded()
                }) { _ in
                    UIView.animate(withDuration: 0.3, animations: {
                        self.para3LeadingLayoutConstraint.constant = self.paraOriginalConstant
                        self.view.layoutIfNeeded()
                    }) { _ in
                        
                    }
                }
            }
        }
    }
    
    @IBAction func startButtonPressed() {
        self.performSegue(withIdentifier: "BeforeWeStartToPlaylistSegue", sender: nil)
    }
}
