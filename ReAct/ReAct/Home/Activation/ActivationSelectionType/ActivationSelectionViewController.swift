//
//  ActivationSelectionViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 09/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class ActivationSelectionViewController: UIViewController {
    
    @IBOutlet var scrollView: UIScrollView!
    private var dataSource: [Category]!
    private var slides: [ActivationSelectorView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = DataRepo.shared.categories
        self.slides = self.createSlides()
        self.setupScrollView(slides: self.slides)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.scrollView.alpha = 1.0
        self.parent?.navigationItem.hidesBackButton = false
    }
    
    private func createSlides() -> [ActivationSelectorView] {
        var slides: [ActivationSelectorView] = []
        for category in self.dataSource {
            let slide: ActivationSelectorView = Bundle.main.loadNibNamed("ActivationSelectorView", owner: self, options: nil)?.first as! ActivationSelectorView
            slide.translatesAutoresizingMaskIntoConstraints = false
            slide.configure(category: category)
            slide.delegate = self
            slides.append(slide)
        }
        return slides
    }
    
    private func setupScrollView(slides : [ActivationSelectorView]) {
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(slides.count), height: self.scrollView.frame.height)
        self.scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            self.scrollView.addSubview(slides[i])
            NSLayoutConstraint.activate([
                slides[i].topAnchor.constraint(equalTo: scrollView.topAnchor),
                slides[i].widthAnchor.constraint(equalTo: scrollView.widthAnchor),
                slides[i].heightAnchor.constraint(equalTo: scrollView.heightAnchor),
                slides[i].bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
            ])
        }
        
        NSLayoutConstraint.activate([
            slides[0].leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            slides[0].trailingAnchor.constraint(equalTo: slides[1].leadingAnchor),
            slides[1].leadingAnchor.constraint(equalTo: slides[0].trailingAnchor),
            slides[1].trailingAnchor.constraint(equalTo: slides[2].leadingAnchor),
            slides[2].leadingAnchor.constraint(equalTo: slides[1].trailingAnchor),
            slides[2].trailingAnchor.constraint(equalTo: slides[3].leadingAnchor),
            slides[3].leadingAnchor.constraint(equalTo: slides[2].trailingAnchor),
            slides[3].trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
        ])
        
        self.view.layoutIfNeeded()
        self.scrollView.layoutIfNeeded()
    }
}

extension ActivationSelectionViewController: ActivationSelectorViewDelegate {
    func categorySelected(category: Category) {
        if let index = self.dataSource.firstIndex(where: { $0.name == category.name }) {
            let slide = self.slides[index]
            let newLabel = UILabel(frame: .zero)
            newLabel.numberOfLines = 0
            newLabel.textAlignment = .center
            newLabel.font = slide.nameLabel.font
            newLabel.textColor = slide.nameLabel.textColor
            newLabel.text = slide.nameLabel.text
            let newFrame = slide.convert(slide.nameLabel.frame, to: self.parent?.view)
            newLabel.frame = newFrame
            slide.nameLabel.alpha = 0.0
            self.parent?.title = ""
            self.parent?.view.addSubview(newLabel)
            self.parent?.navigationItem.hidesBackButton = true
            self.parent?.navigationItem.leftBarButtonItem = nil
            
            UIView.animate(withDuration: 0.5, animations: {
                newLabel.frame = CGRect(x: newLabel.frame.origin.x, y: 50, width: newLabel.frame.width, height: newLabel.frame.height)
                self.view.layoutIfNeeded()
                self.scrollView.alpha = 0.0
                slide.nameLabel.alpha = 1.0
                newLabel.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
            }) { _ in
                self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
                guard let parentView = self.parent as? ActivationViewController else {
                    return
                }
                parentView.selectedCategory = category
                self.parent?.performSegue(withIdentifier: "ActivationToMovermentListSegue", sender: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    newLabel.removeFromSuperview()
                }
            }
            
        }
    }
}
