//
//  ActivationSelectorCollectionViewCell.swift
//  ReAct
//
//  Created by Ian Houghton on 09/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

protocol ActivationSelectorViewDelegate: AnyObject {
    func categorySelected(category: Category)
}

class ActivationSelectorView: UIView {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    weak var delegate: ActivationSelectorViewDelegate?
    private var category: Category!
    
    func configure(category: Category) {
        self.nameLabel.text = category.name
        self.descriptionLabel.text = category.description
        self.category = category
        
        self.heightConstraint.constant = UIDevice.current.hasNotch ? 407 : 300
    }
    
    @IBAction func categorySelectionButtonPressed() {
        self.delegate?.categorySelected(category: self.category)
    }
}
