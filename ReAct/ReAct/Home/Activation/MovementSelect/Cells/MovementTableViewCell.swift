//
//  MovementTableViewCell.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 24/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

protocol MovementTableViewCellDelegate: AnyObject {
    func selectionButtonPressed(movement: Movement)
}

class MovementTableViewCell: UITableViewCell {
    
    @IBOutlet weak var movementImageView: UIImageView!
    @IBOutlet weak var movementNameLabel: UILabel!
    @IBOutlet weak var selectedIcon: UIImageView! {
        didSet {
            self.selectedIcon.alpha = 0.0
        }
    }
    
    private weak var delegate: MovementTableViewCellDelegate?
    private var movement: Movement!
    
    func configure(movement: Movement, selected: Bool, delegate: MovementTableViewCellDelegate) {
        self.movement = movement
        self.delegate = delegate
        self.selectedIcon.alpha = selected ? 1.0 : 0.0
        self.movementNameLabel.text = movement.name
    }
    
    @IBAction func selectButtonToggled() {
        self.delegate?.selectionButtonPressed(movement: self.movement)
    }
}
