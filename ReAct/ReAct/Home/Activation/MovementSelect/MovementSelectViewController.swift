//
//  MovementSelectViewController.swift
//  ReAct
//
//  Created by Ian Layland-Houghton on 24/08/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class MovementSelectViewController: UIViewController {
    
    @IBOutlet var startButton: UIButton! {
        didSet {
            self.startButton.alpha = 0.0
        }
    }
    @IBOutlet var cancelButton: UIButton! {
        didSet {
            self.cancelButton.alpha = 0.0
        }
    }
    @IBOutlet var textField: UITextField! {
        didSet {
            self.textField.alpha = 0.0
            self.textField.setupLeftImage(imageName: "magnifyingGlass")
            self.textField.placeholder = "Search Library"
            self.textField.font = UIFont(name: "AvertaStd-Regular", size: 15.0)!
            self.textField.layer.cornerRadius = 15
            self.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.textField.inputDelegate = self
        }
    }
    @IBOutlet var infoImage: UIImageView! {
        didSet {
            self.infoImage.alpha = 0.0
        }
    }
    @IBOutlet var infoLabel: UILabel! {
        didSet {
            self.infoLabel.alpha = 0.0
        }
    }
    @IBOutlet var tableView: UITableView! {
        didSet {
            self.tableView.estimatedRowHeight = 80
            self.tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    private var selectedMovements: [Movement] = []
    private var movementList: [Movement]!
    private var dataSource: [Movement]!
    private var selectedCategory: Category!
    private var firstLoad: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "AvertaStd-Bold", size: 16.5)!]
        self.navigationItem.title = self.selectedCategory.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.alpha = 1.0
        self.animate()
    }
    
    private func animate() {
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.5, animations: {
            self.infoLabel.alpha = 1.0
            self.infoImage.alpha = 1.0
            self.textField.alpha = 1.0
            self.cancelButton.alpha = 1.0
        }) { _ in
            self.firstLoad = false
        }
    }

    func configure(movements: [Movement], category: Category) {
        self.selectedCategory = category
        self.movementList = movements
        self.dataSource = movements
    }
    
    private func configureStartButton() {
        UIView.animate(withDuration: 0.25) {
            self.startButton.alpha = self.selectedMovements.count > 0 ? 1.0 : 0.0
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.selectedMovements.count > 0 ? 85 : 0, right: 35)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func cancelButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func startButtonPressed() {
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.alpha = 0
            self.navigationController?.navigationBar.alpha = 0.0
            self.infoLabel.alpha = 0
            self.infoImage.alpha = 0
            self.textField.alpha = 0
            self.cancelButton.alpha = 0
        }) { _ in
            do {
                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: self.selectedMovements, requiringSecureCoding: true)
                UserDefaults.standard.set(encodedData, forKey: "ActivationMovements")
            } catch _ {

            }
            
            self.navigationController?.navigationBar.alpha = 1.0
            self.performSegue(withIdentifier: "MovementToBeforeSegue", sender: nil)
        }
    }
}

extension MovementSelectViewController: UITextInputDelegate, UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let searchTerm = textField.text else {
            self.dataSource = self.movementList
            self.tableView.reloadData()
            return
        }
        
        if searchTerm.count > 0 {
            let results = self.movementList.filter{ $0.name.lowercased().contains(searchTerm.lowercased()) }
            self.dataSource = results
        } else {
            self.dataSource = self.movementList
        }
        
        self.tableView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func selectionWillChange(_ textInput: UITextInput?) {
        
    }
    
    func selectionDidChange(_ textInput: UITextInput?) {
        
    }
    
    func textWillChange(_ textInput: UITextInput?) {
        
    }
    
    func textDidChange(_ textInput: UITextInput?) {
        
    }
}

extension MovementSelectViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.firstLoad {
            let animation = AnimationFactory.makeSlideIn(duration: 0.25, delayFactor: 0.15)
            let animator = Animator(animation: animation)
            animator.animate(cell: cell, at: indexPath, in: tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovementTableViewCell
        let movement = self.dataSource[indexPath.row]
        cell.configure(movement: movement, selected: self.selectedMovements.contains(obj: movement), delegate: self)
        return cell
    }
}

extension MovementSelectViewController: MovementTableViewCellDelegate {
    func selectionButtonPressed(movement: Movement) {
        if self.selectedMovements.contains(obj: movement) {
            self.selectedMovements.removeAll{ $0.name == movement.name }
        } else {
            self.selectedMovements.append(movement)
        }
        
        self.configureStartButton()
        self.tableView.reloadData()
    }
}
