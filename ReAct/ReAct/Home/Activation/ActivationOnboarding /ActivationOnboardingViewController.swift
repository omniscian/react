//
//  ActivationOnboardingViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 08/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

protocol ActivationOnboardingViewControllerDelegate {
    func topButtonPressed()
    func bottomButtonPressed()
}

class ActivationOnboardingViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var topButton: BorderedButton!
    @IBOutlet var bottomButton: BorderedButton!
    @IBOutlet var topButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var topButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var bottomButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var bottomButtonTrailingConstraint: NSLayoutConstraint!
    
    private var delegate: ActivationOnboardingViewControllerDelegate?
    private var activationMode: Bool = false
    
    func configure(titleText: String, topButtonText: String, bottomButtonText: String, activationMode: Bool, delegate: ActivationOnboardingViewControllerDelegate?) {
        self.titleLabel.text = titleText
        self.topButton.setTitle(topButtonText, for: .normal)
        self.bottomButton.setTitle(bottomButtonText, for: .normal)
        self.activationMode = activationMode
        self.delegate = delegate
    }
    
    @IBAction func topButtonTouched(sender: AnyObject) {
        if !self.topButton.isSelected {
            self.topButtonLeadingConstraint.constant -= 10
            self.topButtonTrailingConstraint.constant -= 10
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func topButtonPressed(sender: AnyObject) {
        self.topButton.isSelected = !self.topButton.isSelected
        if !self.topButton.isSelected {
            self.topButton.layer.borderColor = UIColor.lightGray.cgColor
            self.topButtonLeadingConstraint.constant += 10
            self.topButtonTrailingConstraint.constant += 10
        } else {
            self.topButton.layer.borderColor = UIColor(red: 55.0/255.0, green: 102.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
            if self.bottomButton.isSelected {
                self.bottomButtonPressed(sender: self.topButton)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.topButton.layer.borderColor = UIColor.lightGray.cgColor
                self.topButtonLeadingConstraint.constant += 10
                self.topButtonTrailingConstraint.constant += 10
                self.topButton.isSelected = false
                self.view.layoutIfNeeded()
                self.delegate?.topButtonPressed()
            }
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func bottomButtonTouched(sender: AnyObject) {
        if !self.bottomButton.isSelected {
            self.bottomButtonLeadingConstraint.constant -= 10
            self.bottomButtonTrailingConstraint.constant -= 10
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }

    
    @IBAction func bottomButtonPressed(sender: AnyObject) {
        self.bottomButton.isSelected = !self.bottomButton.isSelected
        if !self.bottomButton.isSelected {
            self.bottomButton.layer.borderColor = UIColor.lightGray.cgColor
            self.bottomButtonLeadingConstraint.constant += 10
            self.bottomButtonTrailingConstraint.constant += 10
        } else {
            self.bottomButton.layer.borderColor = UIColor(red: 55.0/255.0, green: 102.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
            if self.topButton.isSelected {
                self.topButtonPressed(sender: self.topButton)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.delegate?.bottomButtonPressed()
            }
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}
