//
//  ActivationViewController.swift
//  ReAct
//
//  Created by Ian Houghton on 07/03/2020.
//  Copyright © 2020 YakApps. All rights reserved.
//

import UIKit

class ActivationViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var containerView: UIView!
    
    var selectedCategory: Category?
    
    private var onboardingViewControllers: [UIViewController] = []
    private var activationMode: Bool = true
    private var activationMovements: [Movement]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            if let activationMovementsAsData = UserDefaults.standard.object(forKey: "ActivationMovements") as? Data {
                if let activationMovements = try NSKeyedUnarchiver.unarchivedObject(ofClasses: [Movement.self, NSArray.self], from: activationMovementsAsData) {
                    self.activationMovements = activationMovements as? [Movement]
                }
            }
        } catch (let error)  {

        }

        let onboardingScreen1 = ActivationOnboardingViewController(nibName: "ActivationOnboardingViewController", bundle: nil)
        onboardingScreen1.loadView()
        let onboardingScreen2 = ActivationOnboardingViewController(nibName: "ActivationOnboardingViewController", bundle: nil)
        onboardingScreen2.loadView()
        let activationSelectorVC = ActivationSelectionViewController(nibName: "ActivationSelectionViewController", bundle: nil)
        
        self.onboardingViewControllers.append(onboardingScreen1)
        if !self.activationMode && self.activationMovements?.count ?? 0 > 0 {
            self.onboardingViewControllers.append(onboardingScreen2)
        }
        self.onboardingViewControllers.append(activationSelectorVC)
        
        self.addChildViewControllers()
        
        onboardingScreen1.configure(titleText: "How long do you want to activate for?", topButtonText: "Less than 10 mins", bottomButtonText: "Less than 20 mins", activationMode: self.activationMode, delegate: self)
        onboardingScreen2.configure(titleText: "Use movements from activation?", topButtonText: "Yes", bottomButtonText: "No", activationMode: self.activationMode, delegate: self)
    }
    
    func configure(activationMode: Bool) {
        self.activationMode = activationMode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let backBTN = UIBarButtonItem(image: UIImage(named: "navBackButton"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        self.navigationItem.leftBarButtonItem = backBTN
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    private func addChildViewControllers() {
        var x: CGFloat = 0
        
        for vc in self.onboardingViewControllers {
            vc.view.frame = CGRect(x: x, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            vc.willMove(toParent: self)
            addChild(vc)
            vc.didMove(toParent: self)
            x += view.frame.width
            self.scrollView.addSubview(vc.view)
        }

        self.scrollView.contentSize = CGSize(width: CGFloat(self.onboardingViewControllers.count) * view.frame.width, height: self.scrollView.frame.height)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "ActivationToMovermentListSegue" {
            guard let selectedCategory = self.selectedCategory else {
                return
            }
            let viewController = (segue.destination as? UINavigationController)?.viewControllers[0] as? MovementSelectViewController
            let movements = DataRepo.shared.movementsForCategory(category: selectedCategory)
            viewController?.configure(movements: movements, category: selectedCategory)
        }
    }
    
    private func goToNextScreen() {
        if self.scrollView.contentOffset.x == 0 {
            if self.activationMode {
                self.title = "Select a movement"
            }
            UIView.animate(withDuration: 0.2) {
                self.scrollView.contentOffset.x = self.scrollView.frame.width
            }
        }
        else {
            self.title = "Select a movement"
            UIView.animate(withDuration: 0.2) {
                self.scrollView.contentOffset.x = self.scrollView.frame.width * 2
            }
        }
    }
}

extension ActivationViewController: ActivationOnboardingViewControllerDelegate {
    func topButtonPressed() {
        if self.scrollView.contentOffset.x != 0 && !self.activationMode {
            print("This is where we build a playlist based on selected activations. Use above array.")
        }
        self.goToNextScreen()
    }
    
    func bottomButtonPressed() {
        self.goToNextScreen()
    }
}
